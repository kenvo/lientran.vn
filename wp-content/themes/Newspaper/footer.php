
<!-- Instagram -->

<?php if (td_util::get_option('tds_footer_instagram') == 'show') { ?>

<div class="td-main-content-wrap td-footer-instagram-container">
    <?php
    //get the instagram id from the panel
    $tds_footer_instagram_id = td_util::get_option('tds_footer_instagram_id');
    ?>

    <div class="td-instagram-user">
        <h4 class="td-footer-instagram-title">
            <?php echo  __td('Follow us on Instagram', TD_THEME_NAME); ?>
            <a class="td-footer-instagram-user-link" href="https://www.instagram.com/<?php echo $tds_footer_instagram_id ?>" target="_blank">@<?php echo $tds_footer_instagram_id ?></a>
        </h4>
    </div>

    <?php
    //get the other panel seetings
    $tds_footer_instagram_nr_of_row_images = intval(td_util::get_option('tds_footer_instagram_on_row_images_number'));
    $tds_footer_instagram_nr_of_rows = intval(td_util::get_option('tds_footer_instagram_rows_number'));
    $tds_footer_instagram_img_gap = td_util::get_option('tds_footer_instagram_image_gap');
    $tds_footer_instagram_header = td_util::get_option('tds_footer_instagram_header_section');

    //show the insta block
    echo td_global_blocks::get_instance('td_block_instagram')->render(
        array(
            'instagram_id' => $tds_footer_instagram_id,
            'instagram_header' => /*td_util::get_option('tds_footer_instagram_header_section')*/ 1,
            'instagram_images_per_row' => $tds_footer_instagram_nr_of_row_images,
            'instagram_number_of_rows' => $tds_footer_instagram_nr_of_rows,
            'instagram_margin' => $tds_footer_instagram_img_gap
        )
    );

    ?>
</div>

<?php } ?>


<!-- Footer -->
<?php
if (td_util::get_option('tds_footer') != 'no') {
    td_api_footer_template::_helper_show_footer();
}
?>


<!-- Sub Footer -->
<?php if (td_util::get_option('tds_sub_footer') != 'no') { ?>
    <div class="td-sub-footer-container">
        <div class="td-container">
            <div class="td-pb-row">
                <!-- <div class="td-pb-span7 td-sub-footer-menu"> -->
                <div class="td-pb-span6 td-sub-footer-copy">
                        <?php
                        // wp_nav_menu(array(
                        //     'theme_location' => 'footer-menu',
                        //     'menu_class'=> 'td-subfooter-menu',
                        //     'fallback_cb' => 'td_wp_footer_menu'
                        // ));

                        // //if no menu
                        // function td_wp_footer_menu() {
                        //     //do nothing?
                        // }
                            echo '<p style="margin-bottom: 0; font-size: 12px;">© Copyright '. date("Y") .' by Lien Tran Email: lienhongtran@live.com. All Rights Reserved</p>';
                        ?>
                </div>

                <div class="td-pb-span6 td-sub-footer-copy">
                    <?php
                    // $tds_footer_copyright = stripslashes(td_util::get_option('tds_footer_copyright'));
                    // $tds_footer_copy_symbol = td_util::get_option('tds_footer_copy_symbol');

                    // //show copyright symbol
                    // if ($tds_footer_copy_symbol == '') {
                    //     echo '&copy; ';
                    // }

                    // echo $tds_footer_copyright;
                    if( pll_current_language() == 'vi' ) {
                        echo '
                            <p style="margin-bottom: 0;">
                                <i class="fa fa-user"></i> Người dùng đang online: ' . 
                                do_shortcode('[wpstatistics stat=usersonline]') .
                                '<i style="padding-left: 10px;" class="fa fa-calendar-minus-o"></i> Tổng số lượt truy cập: ' . 
                                (intval( do_shortcode('[wpstatistics stat=visitors time=total]') ) + 29118) . 
                            '</p>';
                    } else {
                        echo '<p style="margin-bottom: 0;">
                                <i class="fa fa-user"></i> Online visitors: ' . 
                                do_shortcode('[wpstatistics stat=usersonline]') .
                                '<i style="padding-left: 10px;" class="fa fa-calendar-minus-o"></i> Total visitors: ' . 
                                ( intval( do_shortcode('[wpstatistics stat=visitors time=total]') ) + 29118) . 
                            '</p>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</div><!--close td-outer-wrap-->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.td-search-wrapper').show();
        jQuery('li.menu-item a.sf-with-ul').css('padding-right', '13px');
        jQuery(window).scroll(function() {
            jQuery('li.menu-item a.sf-with-ul').css('padding-right', '13px');
            // jQuery('.td-search-wrapper').hide();
        });
    });
</script>
<?php wp_footer(); ?>

</body>
</html>